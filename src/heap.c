#include "util.h"
#include <stdlib.h>

void heap_overrun() {
    logstr("- overrun: int a[3]; access a[3];\n");
    int *a = calloc(3, sizeof(int));
    a[0] = 1; a[1] = 2; a[2] = 4;

    // this element is not part of the array on the stack
    int r = a[3];
    logstr("  1: %d, 2: %d, 3: %d 4: %d\n", a[0], a[1], a[2], r);

    free(a);
}

void heap_underrun() {
    logstr("- underrun: int a[3]; access a[-1];\n");
    int *a = calloc(3, sizeof(int));
    a[0] = 1; a[1] = 2; a[2] = 4;

    // access 2 bytes before array:
    int r = a[-1];
    logstr("  1: %d, 2: %d, 3: %d 4: %d\n", a[0], a[1], a[2], r);

    free(a);
}

void heap_segfault() {
    logstr("- segfault: a = 1; ++a = 2; ++a = 3;\n");
    int *a = malloc(sizeof(int));
    *a = 1;
    logstr("  a: %d, %p\n", *a, a);
    *(++a) = 2;
    logstr("  a: %d, %p\n", *a, a);
    a++;
    *a = 3;
    logstr("  a: %d, %p\n", *a, a);

    logstr("  advance pointer 100 bytes\n");
    int b = 0;
    for (int i=0; i<100; i++, a++)
        b = a;
}
