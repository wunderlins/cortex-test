#include "util.h"

void stack_overrun() {
    logstr("- overrun: int a[3]; access a[3];\n");
    int a[3] = {1, 2, 3};
    // this element is not part of the array on the stack
    int r = a[3];
    logstr("  1: %d, 2: %d, 3: %d 4: %d\n", a[0], a[1], a[2], r);
}

void stack_underrun() {
    logstr("- underrun: int a[3]; access a[-1];\n");
    int a[3] = {1, 2, 3};
    // access 2 bytes before array:
    int r = a[-1];
    logstr("  1: %d, 2: %d, 3: %d 4: %d\n", a[0], a[1], a[2], r);
}