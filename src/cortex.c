#include <stdio.h>
#include "util.h"
#include "stack.h"
#include "heap.h"

int main(int argc, char **argv) {
    // put something on the stack
    int i = 1337;

    logstr("===> Testing Stack Functions\n");
    stack_overrun();
    stack_underrun();

    logstr("\n===> Testing Heap Functions\n");
    heap_overrun();
    heap_underrun();
    heap_segfault();

    return 0;
}